'use strict';

var React = require('react');

var ColGroup = React.createClass({
  render: function () {
    var group = this;
    var col = group.props.col;
    var row = group.props.row;
    var attr = col.group.inputs[0].attr;
    var val = row.hasOwnProperty(attr) && row[attr] ? row[attr] : undefined;

    if (!val) return React.createElement(
      'td',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    );

    return React.createElement(
      'td',
      null,
      val
    );
  }
});

module.exports = ColGroup;