'use strict';

var React = require('react');
var numeral = require('numeral');

var ColInteger = React.createClass({
  render: function () {
    var val = this.props.data.value;
    var valStr = String(val !== 0 ? val : 0).trim();
    if (!valStr.length) return React.createElement(
      'td',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    );

    var num = parseFloat(val || 0, 10);
    val = numeral(num).format('(0 a)');

    return React.createElement(
      'td',
      null,
      val
    );
  }
});

module.exports = ColInteger;