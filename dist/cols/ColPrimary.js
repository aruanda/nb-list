'use strict';

var React = require('react');

var ColPrimary = React.createClass({
  render: function () {
    var val = this.props.data.value;

    if (!val) return React.createElement(
      'td',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    );

    val = '000000'.concat(val).slice(-6).trim();

    return React.createElement(
      'td',
      { style: { textAlign: 'center', width: '95px' } },
      val
    );
  }
});

module.exports = ColPrimary;