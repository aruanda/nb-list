'use strict';

var React = require('react');

var ColText = React.createClass({
  render: function () {
    var val = this.props.data.value;
    if (!val) return React.createElement(
      'td',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    );
    return React.createElement('td', { dangerouslySetInnerHTML: { __html: val } });
  }
});

module.exports = ColText;