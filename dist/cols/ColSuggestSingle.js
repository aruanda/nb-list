'use strict';

var React = require('react');

var ColSuggestSingle = React.createClass({
  render: function () {
    var props = this.props;
    var col = props.col;
    var val = props.row[col.attr];

    if (!val) return React.createElement(
      'td',
      { style: { color: '#8A8A8A', textAlign: 'center' } },
      'vazio'
    );

    return React.createElement(
      'td',
      null,
      React.createElement(
        'span',
        { className: 'label label-primary' },
        val.label
      )
    );
  }
});

module.exports = ColSuggestSingle;