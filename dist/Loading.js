'use strict';

var React = require('react');

var Loading = React.createClass({
  render: function () {
    return React.createElement(
      'section',
      null,
      React.createElement(
        'h4',
        null,
        'table.loading'.translate()
      ),
      React.createElement(
        'div',
        { className: 'progress' },
        React.createElement('div', {
          className: 'progress-bar progress-bar-striped active',
          style: { width: '100%' }
        })
      )
    );
  }
});

module.exports = Loading;