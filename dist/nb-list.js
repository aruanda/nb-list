'use strict';

var React = require('react');
var Loading = require('./Loading');

var colTypes = {
  seo: require('./cols/ColSEO'),
  text: require('./cols/ColText'),
  email: require('./cols/ColEmail'),
  group: require('./cols/ColGroup'),
  title: require('./cols/ColString'),
  string: require('./cols/ColString'),
  images: require('./cols/ColImages'),
  integer: require('./cols/ColInteger'),
  primary: require('./cols/ColPrimary'),
  textarea: require('./cols/ColText'),
  customer: require('./cols/ColCustomer'),
  currency: require('./cols/ColCurrency'),
  suggestSingle: require('./cols/ColSuggestSingle'),
  suggestMultiple: require('./cols/ColSuggestMultiple')
};

var ListContent = React.createClass({
  render: function () {
    var trRows;
    var trLoading;

    var props = this.props;
    var rows = props.data;
    var cols = props.cols;
    var loaded = Boolean(props.loaded);
    var othersCols = 1;
    var totalCols = cols.length + othersCols;

    var tableCols = [];

    cols.forEach(function (col) {
      if (!colTypes.hasOwnProperty(col.kind)) return;

      col.label = col.domain.concat('.list.', col.attr).translate();

      var config = { textAlign: 'left' };

      if (col.kind === 'primary') {
        config.textAlign = 'center';
      }

      if (col.kind === 'currency') {
        config.textAlign = 'right';
      }

      tableCols.push(React.createElement(
        'th',
        { key: tableCols.length, style: config },
        col.domain.concat('.list.', col.attr).translate()
      ));
    });

    tableCols.push(React.createElement('th', { key: tableCols.length }));

    if (!loaded) {
      trLoading = React.createElement(
        'tr',
        null,
        React.createElement(
          'td',
          { colSpan: totalCols, style: { textAlign: 'center', backgroundColor: '#fff' } },
          React.createElement(Loading, null)
        )
      );
    }

    if (loaded) {
      var withData = rows && rows.length;
      var withoutData = !withData;

      if (withoutData) {
        trRows = React.createElement(
          'tr',
          null,
          React.createElement(
            'td',
            { colSpan: totalCols, style: { textAlign: 'center', backgroundColor: '#fff' } },
            React.createElement(
              'section',
              null,
              React.createElement(
                'h4',
                null,
                'table.notFound'.translate()
              )
            )
          )
        );
      }

      if (withData) {
        trRows = [];
        rows.forEach(function (row) {
          var tds = [];
          cols.forEach(function (col) {
            if (!colTypes.hasOwnProperty(col.kind)) return;
            var ColType = colTypes[col.kind] || colTypes.string;
            var state = { value: row && row.hasOwnProperty(col.attr) ? row[col.attr] : undefined };

            tds.push(React.createElement(ColType, {
              key: tds.length,
              col: col,
              row: row,
              data: state
            }));
          });

          tds.push(React.createElement(
            'td',
            { key: tds.length, style: { textAlign: 'center', maxWidth: '35px !important' } },
            React.createElement(
              'ul',
              { className: 'list-unstyled' },
              (props.actions || []).btns.map(function (btn, index) {
                var callActionBtn = function () {
                  if (btn.onClick) btn.onClick(row);
                };

                return React.createElement(
                  'li',
                  { key: index, className: 'pull-left', stye: { margin: '0 5px' } },
                  React.createElement(
                    'button',
                    { key: index, onClick: callActionBtn, type: btn.type, title: btn.text.translate(), className: btn.className },
                    React.createElement('span', { className: btn.icon })
                  )
                );
              })
            )
          ));

          trRows.push(React.createElement(
            'tr',
            { key: trRows.length },
            tds
          ));
        });
      }
    }

    return React.createElement(
      'table',
      { className: 'table table-hover' },
      React.createElement(
        'thead',
        null,
        React.createElement(
          'tr',
          null,
          tableCols
        )
      ),
      React.createElement(
        'tbody',
        null,
        trLoading,
        trRows
      )
    );
  }
});

module.exports = ListContent;