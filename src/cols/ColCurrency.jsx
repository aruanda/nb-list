'use strict';

var React = require('react');
var numeral = require('numeral');

var ColCurrency = React.createClass({
  render: function() {
    var val = this.props.data.value;
    var notFilled = [null, undefined, ''].indexOf(val) > -1;
    if (notFilled) return <td style={{ color: '#8A8A8A', textAlign: 'right' }}>vazio</td>;

    var valStr = numeral(val).format('($ 0.00 a)');

    var config = { textAlign: 'right' };

    return <td style={config}>{valStr}</td>;
  }
});

module.exports = ColCurrency;
