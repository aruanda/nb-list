'use strict';

var React = require('react');

var ColImages = React.createClass({
  render: function() {
    var val = this.props.data.value;
    if (!val) return <td style={{ color: '#8A8A8A', textAlign: 'center' }}>vazio</td>;
    return <td>{val}</td>;
  }
});

module.exports = ColImages;
