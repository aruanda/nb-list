'use strict';

var React = require('react');

var ColSuggestMultiple = React.createClass({
  render: function() {
    var props = this.props;
    var col   = props.col;
    var val   = props.row[col.attr];

    if (!val || !val.length) return <td style={{ color: '#8A8A8A' }}>vazio</td>;

      var colors = [
        'label-primary',
        'label-success',
        'label-warning',
        'label-info',
        'label-danger'
      ];

    return (
      <td>
        {val.map(function(row, index) {
          var idx = index + 1;
          var length = colors.length;

          if (idx > length) idx -= Math.ceil(idx / length) * length;

          var cor = colors[idx - 1];

          return (
            <span key={index} className={'label '.concat(cor)} style={{ marginRight: '7px' }}>
              {row.label}
            </span>
          );
        })}
      </td>
    );
  }
});

module.exports = ColSuggestMultiple;
